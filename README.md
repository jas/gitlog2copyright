# gitlog2copyright

This tool will print "Copyright:" lines based on the authorship
information stored in git history in current directory.

Example output when invoked in a git repository:

```
Copyright: 2003-2014 Jane Doe <jane@doe> (17 commits)
           2017      John Doe <john@doe> (1 commit)
           2004-2008 Someone Else <someone@else> (42 commits)
```

The output follows the following properties:

- Lines are ordered by reverse git commit chronology, i.e., the first
  line of output will correspond to the author who made the first
  commit in git history.

  The rationale for this rule is that the first author is likely to
  have started the project, and should be shown first.
  
  Note that the git chronology does not necessarily imply calendar
  chronology; an author may have authored a commit year X but it was
  added to the git history during year X+1.  This script will use X
  for the copyright year information (since that was when the
  contribution was authored and assumed published) but X+1 for
  ordering of the output lines.

- The years shown will reflect the years the person authored commits.
  If contributions were made only during one year it will show as YYYY
  indicating that year.  If the author made commits more than only one
  year, it will show as YYYY-ZZZZ where YYYY will be the earliest year
  and ZZZZ will be the most recent year.

- The email address for a name is the most recently used email.  The
  rationale for this rule is that the most recent email in git history
  is most likely to be the currently actively used email address.

# Author and license

Written by Simon Josefsson and released under the
[AGPLv3+](https://www.gnu.org/licenses/agpl-3.0.html) license, see the
file [COPYING](COPYING).

The implementation was inspired by gnulib's
[gitlog-to-changelog](https://git.savannah.gnu.org/cgit/gnulib.git/tree/build-aux/gitlog-to-changelog)
written by Jim Meyering which is published under the GPLv3+ license,
which is also the reason for the FSF copyright notice above.
